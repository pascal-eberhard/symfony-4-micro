<?php

$tmp = [
    \Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
];

// composer.json require-dev
//if (\class_exists(\Symfony\Bundle\DebugBundle\DebugBundle::class)) {
//    $tmp[\Symfony\Bundle\DebugBundle\DebugBundle::class] = ['dev' => true, 'test' => true];
//}

return $tmp;
